#include <cstdio>
#include <cstring>
#include <vector>
#include <algorithm>

const int maxLen = 500000 + 2; //500k

int n;

char s[maxLen];
int P[maxLen];

std::vector<int> prefSuf;

void makeP(int n)
{
	int b = P[0] = -1;
	for(int i = 1; i <= n; i++)
	{
		while( (b > -1) && (s[b] != s[i-1]) )  b = P[b];
		P[i] = ++b;
	}
}
void makePrefSuf()
{
	int t = n;
	prefSuf.push_back(n);
	while(t = P[t])
	{
		if(t == 0)
			break;
		prefSuf.push_back(t);
	}
	reverse(prefSuf.begin(), prefSuf.end());
 }

void printPref(int n)
{
	for (int i = 0; i < prefSuf.size(); ++i)
	{
		printf("%d ", prefSuf[i]);
	}
	printf("\n");
}

void printP(int n)
{
	for (int i = 0; i <= n; ++i)
	{
		printf("[%d] => %d\n", i, P[i]);
	}
	printf("\n");
}

bool check(int x)
{
	int pp =   0; // pozycja wzorca
	int b  =   0; // długośc prefiksosufiksu
	int last = 0; // ostatnio znaleiony(pozycja)
	for(int i = 0; i < n; i++)
	{
		while(b > -1 && s[b] != s[i]) b = P[b];

		b++;
		if(b < x)
			continue;
		pp = i - b + 1;
		///CHECK ODLEGŁOŚCI 
		if(pp - last > x) // są za daleko od siebie
		{
			return false;
		}
		///////////////////
		last = pp;
		b = P[b];
	}
	return true;  
}

int main(int argc, char const *argv[])
{
	scanf("%s", s);
	n = strlen(s);

	makeP(n);
	makePrefSuf();
	// printPref(n);
	// printP(n);

	std::vector<int>::iterator it = prefSuf.begin();
	while(*it != prefSuf.back())
	{
		// printf("Sprawdzam dla: %d\n", *it);
		if(!check(*it))
		{
			it = lower_bound(it,prefSuf.end(), 2*(*it));
		}
		else
			break;
	}

	printf("%d\n", *it);

	return 0;
}