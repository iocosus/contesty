#include <iostream>
#include <algorithm>

int rura[300000];

using namespace std;

int main(int argc, char **argv)
{
	ios_base::sync_with_stdio(0);
	int n, m;
	cin >> n >> m;

	if(m > n)
	{
		cout << 0 << endl;
		return 0;
	}

	int mn = 2000000000;
	for(int i=0; i<n;i++)
	{
		int tmp;
		cin >> tmp;
		mn = min(mn, tmp);
		rura[i] = mn;
	}

	int depthIndex = n-1;
	int tmp;

	for(int i=0;i<m;i++)
	{
		cin >> tmp;

		if(tmp > rura[0]) // nie ma co szukać i tak nie wpadnie
		{
			cout << 0 << endl;
			return 0;
		}
		for(int j = depthIndex; j >=0; j--)
		{
			if(tmp <= rura[j])
			{
				depthIndex = j-1;
				break;
			}
		}
		if( (depthIndex == -1) && (i+1 < m) )
		{
			cout << 0 << endl;
			return 0;
		}
	}
	cout << depthIndex+2;
	return 0;
}

