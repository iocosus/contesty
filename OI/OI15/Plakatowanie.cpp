#include <cstdio>
#include <stack>

using namespace std;

const int maxLen = 250000; //250k

int t[maxLen];
int n;

int main(int argc, char const *argv[])
{
	scanf("%d", &n);
	for (int i = 0; i < n; ++i)
		scanf("%d %d", t+i , t+i);

	stack<int> s;
	int plakaty = 0;

	for(int i = 0; i < n; i++)
	{
		while(!s.empty() && s.top() > t[i])
			s.pop();
		if(s.empty() || s.top() < t[i])
		{
			s.push(t[i]);
			++plakaty;
		}
	}
	printf("%d\n", plakaty);
	return 0;
}