#include <cstdio>
#include <list>
#include <vector>
#include <stack>
#include <algorithm>

using namespace std;

const int maxN = 100000+100;
const int maxM = 1000000+100;

int n; // ilość vertexów
int m; // ilość edgeów

typedef list< pair<int,int> > pairlist;

pairlist edges[maxM];
pairlist::iterator iters[maxM*2];
bool visited[maxN];


vector<int> output;
stack<int> s;
vector<int> euler;
vector<int> pathLengths;

int iteratorIndex = 0;

void addToStack(int source)
{
	int k;
	while(edges[source].begin() != edges[source].end()) // zostało coś na liście
	{
		s.push(source);
		k = edges[source].begin()->first; // cel krawędzi
		edges[k].erase( iters[ edges[source].begin()->second ] );
		edges[source].pop_front();
		source = k;
	}
}
void addEdge(int a, int b) {
	edges[a].push_front(make_pair(b,iteratorIndex));
	edges[b].push_front(make_pair(a,iteratorIndex+1));

	iters[iteratorIndex]   = edges[b].begin();
	iters[iteratorIndex+1] = edges[a].begin();

	iteratorIndex += 2;
}
bool checkEuler() {
	for(int i = 0; i < n; ++i)
		if(edges[i].size() & 1) // ilosc krawedzi nieparzysta
			return false;
	return true;
}
void makeEuler() {
	for(int i=0;i<n;i++)
	{
		if(edges[i].begin() != edges[i].end()) // jeśli są jakieś elementy
		{
			s.push(i);

			while(!s.empty()) // dopóki jest coś na stosie
			{
				int tmp = s.top();
				s.pop();
				euler.push_back(tmp);
				addToStack(tmp);
			}
		}
	}
}
void cutPaths()
{
	vector<int> tmp;
	for(vector<int>::iterator i = euler.begin(); i != euler.end(); i++)
	{
		if(visited[*i])
		{
			output.push_back(*i);

			int tmpLen = 1;
			int current;
			while(visited[*i])
			{
				current = tmp.back();
				tmp.pop_back();
				output.push_back(current);
				tmpLen++;
				visited[current] = false;
			}
			pathLengths.push_back(tmpLen);
		}
		tmp.push_back(*i);
		visited[*i] = true;
	}
}
void printOutput()
{
	int startIndex = 0;
	printf("%d\n", int(pathLengths.size()));
	for(vector<int>::iterator i = pathLengths.begin(); i != pathLengths.end();i++)
	{
		printf("%d ", *i-1);

		for(int j = 0;j < *i; j++)
		{
			printf("%d ", int(output[startIndex + j])+1);
		}
		startIndex += *i;
		printf("\n");
	}

}

int main(int argc, char const *argv[])
{
	int a,b,q1,q2;

	scanf("%d%d", &n, &m);


	///WCZYTYWANIE
	for(int i = 0; i < m; ++i)
	{
		scanf("%d%d%d%d", &a, &b, &q1, &q2);

		a--; //przepisanie
		b--; // na indeksy zerowe

		if(q1 != q2) // jeżeli trzeba przejść krawędzią
			addEdge(a,b);
	}


	///SPRAWDZENIE CZY JEST CYKL EULERA
	if(!checkEuler())
	{
		printf("NIE\n");
		return 0;
	}
	makeEuler();
	cutPaths();
	printOutput();
	return 0;
}