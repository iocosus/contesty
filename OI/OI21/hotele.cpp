#include <iostream>
#include <vector>
#include <map>
#include <queue>
#include <utility>

using namespace std;

int Binom(int n, int k)
{
	int z =  (k < (n-k)? k : n-k );
	int res = 1;
	for(int i=1;i<=z;i++)
	{
		res *= n-i+1;
		res /= i;
	}
	return res;
}

struct Node
{
	int parent;
	vector<int> el;
	int level;

	void AddEdge(int index)
	{
		el.push_back(index);
	}
	int EdgeCount() { return static_cast<int>(el.size()); }

	Node():level(0){}
};

struct Nd
{
	int vertex;
	int level;
	int parent;

	Nd(int v, int l, int p):
		vertex(v), level(l), parent(p){}
};



int main(int argc, char const *argv[])
{
	ios_base::sync_with_stdio(0);
	std::vector<Node> g(5000);
	int n;
	cin >> n;

	//wczytanie edge'ów
	for (int i = 1; i < n; ++i)
	{
		// cout << "i: " << i << endl;
		int tmp1, tmp2;
		cin >> tmp1 >> tmp2;

		//index od zera
		tmp1--;
		tmp2--;

		g[tmp1].AddEdge(tmp2);
		g[tmp2].AddEdge(tmp1);
	}

	unsigned long long suma = 0;

	//przejście po wszystkich wierzchołkach
	for(int i=0;i<n;i++)
	{
		//nie ma sensu szukać
		if(g[i].EdgeCount() < 3)
			continue;
		// cout << "AKTUALNIE TESTOWANY ELEMENT OSIOWY: [" << i+1 << "] " << endl;
		//pierwszy poziom
		suma += Binom(g[i].EdgeCount(),3);


		//reset odwiedzeń
		for(auto &j : g)
			j.level = 999999999;

		g[i].level = 0;
		g[i].parent = -1;

		queue<int> q;
		// cout << "Pierwszopoziomowe: ";
		for (auto &j: g[i].el)
		{
			g[j].parent = j;
			g[j].level = 1;
			if(j != i)
			{
				q.push(j);
				// cout << j+1 << " ";
			}
		}
		// cout << endl;

		vector< Nd > drzewo;

		//znalezienie poziomów i rodziców
		while(!q.empty())
		{
			int current = q.front();
			q.pop();
			// przejście po vertexach przylegających do current
			for(int j=0; j<g[current].el.size();j++)
			{
				int vertIndex = g[current].el[j];
				//jeszcze nie w drzewie
				if(g[vertIndex].level > g[current].level)
				{
					g[vertIndex].level  = g[current].level+1;
					g[vertIndex].parent = g[current].parent;
					q.push(vertIndex);
					drzewo.push_back( Nd(vertIndex,g[vertIndex].level,g[current].parent) );
				}
			}
		}

		int tempLevel = 2;
		map<int, int> values;

		for(auto &z : drzewo)
		{
			// cout << z.parent << "->" << z.level << endl;
			if(z.level == tempLevel)
			{
				values[z.parent]++;
			}
			else
			{
				vector<int> zliczanie(values.size());
				auto val = values.begin();
				for (int i = 0; i < zliczanie.size(); ++i)
				{
					zliczanie[i] = val->second;
					val++;
				}
				int liczSize = zliczanie.size();
				for (int i1 = 0; i1 < liczSize-2; ++i1)
				{
					for(int i2 = i1+1; i2 < liczSize-1;++i2)
					{
						for(int i3 = i2+1; i3 < liczSize;++i3)
						{
							// cout << "dodaje" << endl;
							suma += zliczanie[i1]*zliczanie[i2]*zliczanie[i3];
						}
					}
				}
				values.clear();
				tempLevel = z.level;
				values[z.parent]++;
			}

		}
		if(!values.empty())
		{
			vector<int> zliczanie(values.size());
			auto val = values.begin();
			for (int i = 0; i < zliczanie.size(); ++i)
			{
				zliczanie[i] = val->second;
				val++;
			}
			int liczSize = zliczanie.size();
			for (int i1 = 0; i1 < liczSize-2; ++i1)
			{
				for(int i2 = i1+1; i2 < liczSize-1;++i2)
				{
					for(int i3 = i2+1; i3 < liczSize;++i3)
					{
						// cout << "dodaje" << endl;
						suma += zliczanie[i1]*zliczanie[i2]*zliczanie[i3];
					}
				}
			}

		}
	}
	cout << suma << endl;
}