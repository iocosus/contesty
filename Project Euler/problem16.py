import math
def sumOfDigits(base, exp):

	numOfDigits = int(math.ceil(exp * math.log10(base)))

	digits = [0] * numOfDigits

	digits[0] = base
	currentExp = 1

	while currentExp < exp:
		currentExp += 1
		carry = 0
		for i in range(len(digits)):
			num = base* digits[i] + carry
			digits[i] = num % 10
			carry = num / 10

	sum = 0

	for i in digits:
		sum += i

	return sum

base = 2
exp = 1000
print sumOfDigits(2,1000)