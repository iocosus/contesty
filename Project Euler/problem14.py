
def Collatz(n):
	counter = 1

	while n != 1:

		if n%2 == 0:
			n /= 2
		else:
			n *= 3
			n += 1
		counter += 1

	return counter

mx = 0
index = 0

for i in xrange(1,1000000):
	k = Collatz(i)
	if k > mx:
		mx = k
		index = i

print index

