dig1 = [1,2,3,4,5,6,7,8,9,10]
dig2 = ["one","two","three","four","five","six","seven","eight","nine","ten"]

dig = dict(zip(dig1,dig2))

teens1 = [11,12,13,14,15,16,17,18,19]
teens2 = ["eleven","twelve","thirteen","fourteen","fifteen","sixteen","seventeen","eighteen","nineteen"]

teens = dict(zip(teens1,teens2))

ties1 = [2,3,4,5,6,7,8,9]
ties2 = ["twenty","thirty","forty","fifty","sixty","seventy","eighty","ninety"]

ties = dict(zip(ties1,ties2))

def translate(n):
	if(n == 1000):
		return "onethousand"
	if(n <=10 ):
		return dig[n]
	if(n <= 19):
		return teens[n]

	a = int((str(n))[0])
	b = int((str(n))[1])

	if(n < 100):
		if(b != 0):
			return ties[a] + dig[b]
		else:
			return ties[a]

	c = int(str(n)[2])
	if( int(n/100)*100 == n):
		return dig[a] + "hundred"

	lt = n%100

	st = str(dig[a]) + "hundredand"

	if(lt <= 10 ):
		return st + dig[lt]
	if(lt <= 19):
		return st + teens[lt]

	if(lt < 100):
		if(c != 0):
			return st + ties[b] + dig[c]
		else:
			return st + ties[b]



sum = 0

for i in xrange(1,1000+1):
	sum += len(translate(i))

print sum