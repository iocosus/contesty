def isPrime(n):
	m = 2
	while(m**2 <= n):
		if (n%m == 0):
			return False
		m += 1
	return True

sum = 0
for i in xrange(2,2000000):
	if isPrime(i):
		sum += i

print sum
