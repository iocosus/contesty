
memo = {0:0,1:1}
sum = 0
def fib(n):
	if n not in memo:
		memo[n] = fib(n-1) + fib(n-2)
	return memo[n]

for x in xrange(1,34):
	f = fib(x)
	print f
	if(f%2 == 0):
		sum += f

print(sum)

