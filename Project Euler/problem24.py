
def rev(lst):
	return [i for i in reversed(lst)]

def next_permutation(a):
	
	k = -1
	for i in xrange(0,len(a)-1):
		if(a[i] < a[i+1]):
			k = i
	# print "k = ", k
	if k == -1:
		return False

	l = 0
	for i in xrange(0,len(a)):
		if(a[k] < a[i]):
			l = i
	# print "l = ", l

	
	a[l], a[k] = a[k], a[l]
	

	
	re = rev(a[k+1:])

	for i in xrange(k+1,len(a)):
			a[i] = re[i-(k+1)]	
	# print "Po: ", (a)

	return a

s = [0,1,2,3,4,5,6,7,8,9]

for i in xrange(1,1000000):
	s = next_permutation(s)

for i in s:
	print i,

