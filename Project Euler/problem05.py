def gcd(a,b):
	while b != 0:
		c = a%b
		a = b
		b = c
	return a

def lcm(a,b):
	return (a*b)/gcd(a,b)

l = 1
for x in xrange(2,21):
	l = lcm(l,x)
print l
	