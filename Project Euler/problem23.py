import math

def sumOfDivisors(n):
	sum = 0
	r = int(math.sqrt(n))

	if (r*r == n):
		sum += r
		r -= 1

	if (n%2 != 0):
		for i in xrange(1,r+1,2):
			if (n%i == 0):
				sum += (i + n/i)
	else:
		for i in xrange(1,r+1):
			if (n%i == 0):
				sum += (i + n/i)
	return sum-n


AbundantNumbers = []

for x in xrange(1,28123):
	if (sumOfDivisors(x) > x):
		AbundantNumbers.append(x)

print AbundantNumbers