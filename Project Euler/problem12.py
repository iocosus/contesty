def CountDivisors(n):
	counter = 2
	i = 2
	while (i**2 <= n):
		if (n%i == 0):
			counter +=2
		i +=1
	return counter

add = 0
num = 0

add += 1
num += add

while CountDivisors(num) <= 500:
	add += 1
	num += add

print num