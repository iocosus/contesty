number = 600851475143

def LPF(n):

	lf = 1

	while n%2 == 0:
		lf = 2
		n /= 2

	p = 3

	while n != 1:
		while n%p == 0:
			lf = p
			n = n/p

		p += 2

	return lf

print (LPF(number))