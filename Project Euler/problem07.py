primes = []

def isPrime(n):
	m = 2
	while(m**2 <= n):
		if (n%m == 0):
			return False
		m += 1
	return True

i = 2

while len(primes)<10001:
	if isPrime(i):
		primes.append(i)
	i += 1

print primes[10000]

# for item in primes:
# 	print item