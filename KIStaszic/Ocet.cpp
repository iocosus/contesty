#include <cstdio>
#include <set>
using namespace std;


int sum, count;
multiset<int> bottles;


int gcd(int a, int b)
{
	while(b != 0)
	{
		int c = a%b;
		a = b;
		b = c;
	}
	return a;
}
void printAVG()
{
	int nwd = gcd(sum, count);
	printf("%d/%d\n", sum/nwd, count/nwd);
}
void newBottle(int b)
{
	sum += b;
	++count;
	bottles.insert(b);
}
void getCheapest()
{
	int val = *bottles.begin();
	bottles.erase(bottles.begin());

	--count;
	sum -= val;

	printf("%d\n", val);
}
void getExpensivest()
{
	int val = *(--bottles.end());
	bottles.erase(--bottles.end());

	--count;
	sum -= val;

	printf("%d\n", val);
}

int main()
{
	int n, tmp;
	scanf("%d", &n);

	while(n--)
	{
		int wyb;
		scanf("%d", &wyb);
		switch(wyb)
		{
			case 0: // zapytanie o srednia cene
				printAVG();
				break;
			case 1: // nowa butelka
				scanf("%d", &tmp);
				newBottle(tmp);
				break;
			case 2:
				getCheapest();
				break;
			case 3:
				getExpensivest();
				break;
		}
	}

}
