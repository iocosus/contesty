#include <cstdio>

typedef unsigned long long ull;

ull gcd(ull a, ull b)
{
	while(b != 0)
	{
		ull c = a%b;
		a = b;
		b = c;
	}
	return a;
}


int main()
{
	int n;
	ull a,b;
	scanf("%d", &n);
	while(n--)
	{
		scanf("%lld%lld", &a, &b);
		while(1)
		{
			ull gc = gcd(a,b);
			if(gc == 1)
				break;
			ull gc2 = gc*gc;
			if(gc2 <= b)
			{
				if(b % gc2 == 0)
				{
					a /= gc;
					b /= gc2;
					continue;
				}
			}
			a /= gc;
			b /= gc;
		}
		printf("%lld %lld\n", a, b);
	}
}
