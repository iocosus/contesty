#include <cstdio>

typedef unsigned long long ull;

const int max = 1000001;

char tab[max];
ull  hash[max];
ull  pow[max];

int n, m;

ull x = 1000000007;

void makeHash(int n)
{
   pow[0] = 1;
   hash[n] = tab[n];
   for(int i=n-1; i >=0 ; i--)
      hash[i] = hash[i+1]*x + tab[i];

   for(int i=1; i < n; i++)
      pow[i] = pow[i-1]*x;
}

ull getHash(int a, int b)
{
   return hash[a] - hash[b+1]*pow[b-a+1];
}

int main()
{
   scanf("%d", &n);
   scanf("%s", tab+1);

   makeHash(n);
   
   scanf("%d", &m);
   int a, b;
   for(int i = 0; i < m; i++)
   {
      scanf("%d%d", &a, &b);
      if(getHash(a, (a+b)/2) == getHash( (a+b)/2 +1, b) )
         printf("TAK\n");
      else
         printf("NIE\n");
   }
}
