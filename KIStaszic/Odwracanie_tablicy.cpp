#include <cstdio>

const int mSize = 10000;

int tab[mSize];

int main()
{
	int n;
	scanf("%d", &n);
	for(int i=0; i < n; i++)
		scanf("%d", tab+i);
	for(int i=n-1;i>=0;i--)
		printf("%d ", tab[i]);
}
