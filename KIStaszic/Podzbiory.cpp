#include <cstdio>

#define pc putchar_unlocked

const int mSize = 16;

char tab[mSize];

void print(int n) // n - ile liczb od
{
	for(int i = mSize - n; i < mSize;i++)
		pc(tab[i] + '0');
	pc('\n');
}

void increment()
{
	tab[mSize-1]++;
	for(int i = mSize-1; i > 0; i--)
	{		
		if(tab[i] > 1)		
		{
			tab[i] = 0;
			tab[i-1]++;
		}
	}
}

int main()
{
	int n;
	scanf("%d", &n);
	
	do
	{
		print(n);
		increment();
	}
	while(!tab[mSize-n-1]);
	
}
