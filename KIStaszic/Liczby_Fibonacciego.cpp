#include <cstdio>
#include <algorithm>
const int maxLen = 300;

typedef unsigned long long ull;

ull fibmemo[100];

ull getFibonacci(int n)
{
	if(fibmemo[n] == 0)
	{
		fibmemo[n] = getFibonacci(n-1)+getFibonacci(n-2);
		return fibmemo[n];
	}
	return fibmemo[n];
}


char arr[3][maxLen];
void reset(int num)
{
	for(int i = 0; i < maxLen; i++)
		arr[num][i] = 0;
}

void add(int a, int b, int c)
{
	reset(c);
	for(int i = 0; i < maxLen;i++)
	{
		arr[c][i]   += arr[a][i]+arr[b][i];
		arr[c][i+1] += arr[c][i]/10;
		arr[c][i]   %= 10;
	}
}



int n;

int main()
{
	fibmemo[1] = fibmemo[2] = 1;
	arr[0][0] = 1;
	arr[1][0] = 1;
	scanf("%d", &n);
/*	if(n <= 90)
	{
		printf("%lld\n", getFibonacci(n));
		return 0;
	}*/
	n-=2; 
	int ptra = 0;
	int ptrb = 1;
	int ptrc = 2;
	for(int i = 0; i < n; i++)
	{
		add(ptra, ptrb, ptrc);
		std::swap(ptrb, ptrc);
		std::swap(ptra, ptrc);
	}
	int i = maxLen-1;
	for(; i >=0; i--)
		if(arr[ptrb][i] != 0)
			break;
	for(;i>=0;i--)
		putchar(arr[ptrb][i]+'0');
}
