#include <cstdio>
#include <cstring>
#include <algorithm>

using namespace std;

const int mx = 1000000;
char wyb[100];
int n;


int   id[mx];
int   iq[mx];
int mass[mx];

int iq_min[mx];
int iq_max[mx];


int Find(int x) // znajdowanie ojca
{
	while(x != id[x]) x = id[x];
	return x;
}
void Union(int a, int b)
{
	int fa = Find(a);
	int fb = Find(b);

	if(fa == fb)
		return;

	if(mass[fa] < mass[fb]) {
		mass[fb] += mass[fa];
		id[fa] = fb;
		iq_min[fb] = min(iq_min[fb], iq_min[fa]);
		iq_max[fb] = max(iq_max[fb], iq_max[fa]);
	}
	else {
		mass[fa] += mass[fb];
		id[fb] = fa;
		iq_min[fa] = min(iq_min[fa], iq_min[fb]);
		iq_max[fa] = max(iq_max[fa], iq_max[fb]);
	}
}

int main()
{
	scanf("%d", &n);
	for(int i=0; i < n;i++) // INICJALIZACJA
		id[i] = i;

	for(int i=0; i < n; i++) // WCZYTANIE MAS I IQ
	{
		scanf("%d%d", mass+i, iq+i);
		iq_max[i] = iq_min[i] = iq[i];
	}
	

	while(scanf("%s", wyb) == 1)
	{
		int a, b;
		if(strcmp(wyb, "JOIN") == 0)
		{
			//printf("Łączę a i b\n");
			scanf("%d%d", &a, &b);
			Union(--a, --b);
		}
		else if(strcmp(wyb, "IQ_MIN") == 0)
		{
			scanf("%d", &a);		
			printf("%d\n", iq_min[Find(--a)]);
		}
		else if(strcmp(wyb, "IQ_MAX") == 0)
		{
			scanf("%d", &a);
			printf("%d\n", iq_max[Find(--a)]);
		}
		else // MASA
		{
			scanf("%d", &a);
			printf("%d\n", mass[Find(--a)]);
		}
	}
}
