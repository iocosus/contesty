#include <cstdio>
#include <cstring>
#include <algorithm>

using namespace std;

typedef unsigned int ull;
typedef unsigned int uint;

const uint maxLen = 500000+1; //500k

ull x = 1297847; // 1e9 + 33
uint  n; // dlugosc napisu
char s[maxLen];

ull hL[maxLen];
ull hR[maxLen];
ull pw[maxLen];


void calcPowers(uint &n)
{
	pw[0] = 1; pw[1] = x;
	for(uint i = 2; i <= n; i++)
		pw[i] = pw[i-1]*x;
}
void hashLeft() // od lewej do prawej
{
	hL[0] = s[0];
	for (uint i = 1; i < n; ++i)
		hL[i] = hL[i-1]*x + s[i];
}
void hashRight() // od prawej do lewej
{
	hR[n-1] = s[n-1];
	for(int i = n-2; i >= 0; --i)
		hR[i] = hR[i+1]*x + s[i];
}

ull getHashLeft(uint &a, uint &b) {
	return hL[b] - hL[a-1]*pw[b-a+1];
}

ull getHashRight(uint &a, uint &b) {
	return hR[a] - hR[b+1]*pw[b-a+1];
}

bool isPalindrome(uint a, uint b) {
	return getHashRight(a,b) == getHashLeft(a,b);
}

int main()
{
	uint a, b;
	scanf("%s", s);

	n = strlen(s);
	calcPowers(n);

	hashLeft();
	hashRight();

	unsigned long long count = 0;
	count += n; // jednoliterowe
	for(uint i = 0; i < n; ++i) // palindromy nieparzyste
	{
		a = 0;
		b = std::min(i,n-i-1);

		if(i > 0 && i < n-1)  // szybkie porównanie liter obok środka
			if(  (s[i-1] != s[i+1]) )
				continue;

		//znajdowanie najwiekszego bedacego palindromem
		while(a < b)
		{
			uint mid = (a+b)>>1;
			if(isPalindrome(i-mid, i+mid))
				a = mid;
			else
				b = mid;

			if(b-a == 1) // hack, który unika pętli nieskończonej
			{
				if(isPalindrome(i-b, i+b)) // tak na wszelki wypadek
					a = b;
				break;
			}
		}
		count += a;
	}
	for (uint i = 1; i < n; ++i) // palindromy parzyste
	{
		a = 0;
		b = std::min(i,n-i+1);
		if(s[i] != s[i-1])
			continue;
		//znajdowanie najwiekszego bedacego palindromem
		while(a < b)
		{
			uint mid = (a+b)>>1;
			if(isPalindrome(i-mid, i+mid-1))
				a = mid;
			else
				b = mid;
			if(b-a == 1)
			{
				if(isPalindrome(i-b, i+b-1))
					a = b;
				break;
			}
		}
		count += a;
	}

	printf("%llu\n", count);
}
