#include <cstdio>
#include <algorithm>

typedef unsigned long long ull;

int modulus = 123456789;

struct TBT
{
	ull A,B,C,D;
	TBT operator*(const TBT &b)
	{
		TBT c;
		c.A = this->A*b.A + this->B*b.C;
		c.B = this->A*b.B + this->B*b.D;
		c.C = this->C*b.A + this->D*b.C;
		c.D = this->C*b.B + this->D*b.D;

		c.A %= modulus;
		c.B %= modulus;
		c.C %= modulus;
		c.D %= modulus;
		return c;
	}
};

TBT pow(TBT mtx, int n)
{
	if(n == 1)
		return mtx;
	if(n % 2 == 0) // parzysta
		return pow(mtx*mtx, n/2);
	else
		return mtx*pow(mtx*mtx,n/2 );
}

int main()
{
	int a=1, b=1;
	int n;
	scanf("%d", &n);
	if(n == 1)
	{
		printf("1\n");
		return 0;
	}
	TBT fib;
	fib.A = fib.B = fib.C = 1;
	fib.D = 0;
	fib = pow(fib, n-1);
	printf("%lld\n", fib.A);
}
