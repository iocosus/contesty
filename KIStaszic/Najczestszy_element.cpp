#include <map>
#include <cstdio>

using namespace std;

typedef map<int, int>::reverse_iterator Itr;

int main()
{
	map<int, int> m;
	int n;
	scanf("%d", &n);
	while(n--)
	{
		int tmp;
		scanf("%d", &tmp);
		m[tmp]++;
	}
	int countmax = -1;
	int number   = -1;
	for(Itr it = m.rbegin(); it != m.rend();it++)
		if(it->second >= countmax)
		{
			countmax = it->second;
			number   = it->first;
		}
	printf("%d", number);
}
