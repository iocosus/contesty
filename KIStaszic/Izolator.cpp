#include <cstdio>
#include <algorithm>

int t[100000];
int o[100000];
int main()
{
	int n, tmp;
	int izo = 0;
	scanf("%d", &n);
	for(int i=0; i < n; i++)
	{
		scanf("%d", &tmp);
		t[i] = tmp;
		izo += tmp;
	}
	std::sort(t, t+n);
	
	int l = 0, p = n-1;
	int j = 0;
	bool min = true;
	while(l <= p)
	{
		if(min)
			o[j++] = t[l++];
		else
			o[j++] = t[p--];
		min  = !min;
	}
	for(int i=1; i < n; i++)
		izo += std::max(0, o[i]-o[i-1]);
	printf("%d", izo);
}
