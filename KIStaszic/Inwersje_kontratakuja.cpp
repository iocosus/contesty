#include <cstdio>

typedef unsigned long long ull;

const int maxN = 60000; //60k
int n;
int tab[maxN];
int aux[maxN];

ull merge(int *t, int p, int mid, int q)
{
	ull counter = 0;
	int i = 0;
	int ptr1 = p;
	int ptr2 = mid+1;
	while(ptr1 <= mid && ptr2 <= q)
	{
		if(tab[ptr1] <= tab[ptr2])
			aux[i++] = tab[ptr1++];
		else
		{

			counter += mid-ptr1+1;
			aux[i++] = tab[ptr2++];
		}
	}
	while(ptr1 <= mid) aux[i++] = tab[ptr1++];
	while(ptr2 <= q) aux[i++] = tab[ptr2++];
	for(int j = 0; j < i;j++)
		tab[p+j] = aux[j];
	return counter;
}

ull mergeSort(int *t, int p, int q)
{
	if(p < q)
	{
		ull res = 0;
		int mid = (p+q)>>1;
		res += mergeSort(t, p, mid);
		res += mergeSort(t, mid+1, q);
		res += merge(t, p, mid, q);
		return res;
	}
	return 0;
}

int main()
{
	scanf("%d", &n);
	for(int i = 0; i < n; i++)
		scanf("%d", tab+i);
	ull res = mergeSort(tab, 0, n-1);
	printf("%lld", res);
}
