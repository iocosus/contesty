#include <cstdio>

const int max = 10000; // 10k

int tab[max];

void insertionSort(int n)
{
	for(int i = 1; i < n; i++)
	{
		int key = tab[i];
		int j = i-1;
		while(j >= 0 && tab[j] < key) tab[j+1] = tab[j--];
		j++;
		tab[j] = key;
	}
}

int main()
{
	int n;
	scanf("%d", &n);
	for(int i=0; i < n; ++i)
		scanf("%d", tab+i);
	
	insertionSort(n);

	for(int i=0; i < n; ++i)
		printf("%d ", tab[i]);
}
