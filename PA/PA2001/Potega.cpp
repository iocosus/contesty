#include <iostream>
#include <string>
using namespace std;

int main() {
	int n;
	int tab[] = {2,4,8,6};
	string s;
	cin >> s;
	if(s.size() < 2)
	{
		if(s[0] == '0')
		{
			cout << 1;
			return 0;
		}
		int t = s[0]-'0'-1;
		cout << tab[t%4];
		return 0;
	}
	n = s[s.size()-2]-'0';
	n *= 10;
	n += s[s.size()-1]-'0';
	n--;
	if(n == -1)
		n = 3;
	cout << tab[n%4];
	return 0;
}
