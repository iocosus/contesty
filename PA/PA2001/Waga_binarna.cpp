#include <cstdio>
#include <algorithm>

struct El {
	int l;
	int m;
	double get() const{ return double(l)/(1<<m); }
	bool operator<(const El& b) const
	{
		if(this->get() == b.get())
			return this->m < b.m;
		return this->get() < b.get();
	}
};

El tab[20000];

int main()
{
	int n;
	scanf("%d", &n);
	for(int i = 0; i < n; i++)
		scanf("%d%d", &tab[i].l, &tab[i].m);
	std::sort(tab, tab+n);
	for(int i = 0; i < n; i++)
		printf("%d %d\n", tab[i].l, tab[i].m);

}
