#include <cstdio>
#include <map>

using namespace std;

typedef map<int, int>::iterator mIter;


void print(map<int, int> m)
{
	for(mIter i = m.begin(); i != m.end(); i++)
	{	
		printf("%d => %d\n", i->first, i->second);
	}
	putchar('\n');
}

int main()
{
	map<int, int> m;
	int n, tmp;
	scanf("%d", &n);
	for(int i = 0; i < n; i++)
	{
		scanf("%d", &tmp);
		m[tmp]++;
	}
	
	mIter it = m.begin();
	while(it != m.end())
	{
		int val = it->second;
		int dest = (it->first)*2;
		
		if(val % 2 == 0)
		{
			m[dest] += val/2;
			mIter cp = it;
			it++;
			m.erase(cp);
			continue;
		}
		else if(val != 1)
		{
			m[dest] += val/2;
			it->second = 1;
			it++;
			continue;
		}
		it++;
	}


	printf("%d\n", int(m.size()));
}
