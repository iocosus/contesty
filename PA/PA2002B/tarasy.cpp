#include <cstdio>
#include <algorithm>

using namespace std;

int n, k;
int tab[20000];

int process()
{
	int j = k;
	int start  = 0;
	int end    = 0;

	int mx = 1;

	for(int i=1; i < n; i++)
	{
		while(j < 0)
		{
			int dt = tab[start+1] - tab[start];
		
			start++;			

			if(dt <= 0)
				continue;
			else
				j += dt;
		}
		end++;		
		int dt = tab[end] - tab[end-1];

		if(dt > 0)
			j -= dt;
		
		if(j >= 0)
			mx = max(mx, end-start+1);
	}
	return mx;
}

int main()
{
	scanf("%d%d", &n, &k);

	for(int i = 0; i < n; i++)
		scanf("%d", tab+i);
	
	int result = process();

	reverse(tab, tab+n);
	
	result = max(process(), result);

	printf("%d", result);
}

