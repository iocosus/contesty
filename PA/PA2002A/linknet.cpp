#include <cstdio>
#include <utility>
#include <algorithm>

using namespace std;

pair<float,bool> points[200000];

int main(int argc, char const *argv[])
{
	unsigned int n;
	scanf("%d", &n);

	n <<= 1;

	for(int i = 0; i < n; i += 2)
	{
		scanf("%f%f", &points[i].first, &points[i+1].first);

		points[i+1].second  = true;
		points[i+1].first  +=  0.5;
	}
	sort(points, points+n);

	// for(int i=0;i<n;i++)
	// {
	// 	printf("%f => %d\n", points[i].first, points[i].second);
	// }
	int max     = 0;
	int current = 0;

	for(int i = 0; i < n; i++)
	{
		if(points[i].second == 0)
			current++;
		else
			current--;

		if(current > max)
			max = current;
	}
	printf("%d\n", max);
	return 0;
}