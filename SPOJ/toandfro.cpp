#include <cstdio>
#include <cstring>
#include <algorithm>

const int M_SIZE = 200+10;

char tab[M_SIZE];

inline void reverse(char *t, int i, int j)
{
	while(i<j) std::swap(t[i++],t[j--]);
}

int main()
{
	int n;
	while(scanf("%d", &n))
	{
		if( n == 0)
			break;
		scanf("%s", tab);
		int len = strlen(tab);
		
		for(int i = n; i < len; i += n*2)
			reverse(tab, i, i+n-1);
		//printf("%s\n", tab);
		for(int j = 0; j < n; j++)
		for(int i = j; i < len; i+= n)
		{
			putchar(tab[i]);
		}	
		putchar('\n');
	}
}
