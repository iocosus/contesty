#!/usr/bin/env python
import sys

f = open(sys.argv[1], "r")
s = f.read()

fq = {
		'A': 0,
		'C': 0,
		'G': 0,
		'T': 0
	}

for i in s:
	if not i == '\n':
		fq[i] += 1

print fq['A'], fq['C'], fq['G'], fq['T']
